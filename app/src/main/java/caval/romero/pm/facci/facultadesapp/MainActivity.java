package caval.romero.pm.facci.facultadesapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import org.json.JSONArray;

import java.util.ArrayList;

import CapaNegocio.Mensajes;

import static CapaNegocio.Servidor.servicio;
import static CapaNegocio.Validaciones.fCadena;

public class MainActivity extends AppCompatActivity {

    RecyclerView RV_facultades;
    ArrayList <String[]>  lista_facultades;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**Declara un cuadro de progreso*/
        pDialog=new ProgressDialog(MainActivity.this);

        /**Declara el recyclerView*/
        RV_facultades= findViewById(R.id.RV_facultades);
        RV_facultades.setLayoutManager(new LinearLayoutManager(this));

        /**Muestra un mensaje de que está cargando*/
        Mensajes.cargando("Obteniendo...",pDialog);

        /**Usa Volley para hacer GET*/
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, servicio("/facultades"), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray facultad) {

                /**Cierra el dialogo de cargando*/
                if (pDialog != null)
                    pDialog.dismiss();

                /**Muestra los datos en en RecyclerView*/
                mostrarDatos(facultad+"");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                /**Si existe un error lo informa*/
                Mensajes.mostrarMensaje("Error", error.toString(), pDialog, MainActivity.this);
            }
        });
        Volley.newRequestQueue(this).add(jsonArrayRequest);
    }

    private void mostrarDatos(String facultad) {
        JsonArray A_facultad=new JsonParser().parse(facultad).getAsJsonArray();
        for(int i =0 ; i < A_facultad.size(); i++) {
            /**Remplaza atributo por el correcto*/
            String id = fCadena(A_facultad, "id", i);
            String foto = fCadena(A_facultad, "foto", i);
            String nombre = fCadena(A_facultad, "nombre", i);
            String cantidad = fCadena(A_facultad, "oferta", i);
            lista_facultades.add(new String[]{id, nombre, cantidad, foto});
        }

        /**Aqui añade lo encontrado en la URL al recyclerview*/
        AdaptadorFacultades adaptadorFacultades= new AdaptadorFacultades(lista_facultades);

        /**Cuando le hace click a un item*/
        adaptadorFacultades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**Obtiene la ubicación del elemento*/
                int id= RV_facultades.getChildAdapterPosition(v);

                /**Crea un nuevo intent para abrir */
                Intent nuevaFacultad= new Intent(MainActivity.this, Facultad.class);

                /**Envia el id de la facultad*/
                nuevaFacultad.putExtra("id", lista_facultades.get(id)[0]);

                /**Abre la nueva actividad*/
                startActivity(nuevaFacultad);
        }
        });
        RV_facultades.setAdapter(adaptadorFacultades);
    }
}
