package caval.romero.pm.facci.facultadesapp;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptadorFacultades extends RecyclerView.Adapter<AdaptadorFacultades.ViewHolderDatos> implements View.OnClickListener {

    ArrayList<String[]> listaDatos;

    private View.OnClickListener listener;

    public AdaptadorFacultades(ArrayList<String[]> listaDatos) {
        this.listaDatos = listaDatos;
    }

    @Override
    public ViewHolderDatos onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.facultades, null, false);
        view.setOnClickListener(this);
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderDatos holder, int position) {
        holder.nombre.setText(listaDatos.get(position)[1]);
        holder.cantidad.setText(listaDatos.get(position)[2]);
        holder.imagen.setImageURI(Uri.parse(listaDatos.get(position)[3]));
    }


    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null){
            listener.onClick(v);
        }
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {
        ImageView imagen;
        TextView nombre, cantidad;
        public ViewHolderDatos(View itemView) {
            super(itemView);
            imagen= itemView.findViewById(R.id.IV_foto_carrera);
            nombre = itemView.findViewById(R.id.TV_nombre);
            cantidad = itemView.findViewById(R.id.TV_oferta);
        }
    }
}
