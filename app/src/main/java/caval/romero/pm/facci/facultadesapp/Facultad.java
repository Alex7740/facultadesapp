package caval.romero.pm.facci.facultadesapp;

import android.app.ProgressDialog;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import org.json.JSONArray;

import CapaNegocio.Mensajes;

import static CapaNegocio.Servidor.servicio;
import static CapaNegocio.Validaciones.fCadena;

public class Facultad extends AppCompatActivity {
    String id= "";
    ProgressDialog pDialog;
    TextView TV_nombre_facultad, TV_ofertados;
    ImageView IV_foto_facultad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facultad);
        id = getIntent().getStringExtra("id");
        /**Declara un cuadro de progreso*/
        pDialog=new ProgressDialog(Facultad.this);

        /**Muestra un mensaje de que está cargando*/
        Mensajes.cargando("Obteniendo...",pDialog);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, servicio("/facultad/"+id), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray facultad) {

                /**Cierra el dialogo de cargando*/
                if (pDialog != null)
                    pDialog.dismiss();

                asignarDatos(facultad+"");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                /**Si existe un error lo informa*/
                Mensajes.mostrarMensaje("Error", error.toString(), pDialog, Facultad.this);
            }
        });
        Volley.newRequestQueue(this).add(jsonArrayRequest);
    }

    private void asignarDatos(String facultad) {
        JsonArray A_facultad=new JsonParser().parse(facultad).getAsJsonArray();
        String foto="", nombre="", cantidad="";
        for(int i =0 ; i < A_facultad.size(); i++) {
            /**Remplaza atributo por el correcto*/
            foto = fCadena(A_facultad, "foto", i);
            nombre = fCadena(A_facultad, "nombre", i);
            cantidad = fCadena(A_facultad, "oferta", i);
        }
        /**Muestra los datos en una nueva actividad*/
        TV_nombre_facultad.setText(nombre);
        TV_ofertados.setText(cantidad);
        IV_foto_facultad.setImageURI(Uri.parse(foto));
    }
}
