package CapaNegocio;

import com.google.gson.JsonArray;

public class Validaciones {
    public static String fCadena(JsonArray Aproductos, String atributo, int index){
        String elemento=Aproductos.get(index).getAsJsonObject().get(atributo).toString().toUpperCase();
        return elemento.substring(1,elemento.length()-1);
    }
}
